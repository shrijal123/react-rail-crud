5.times do
  Assetitem.create({
    item: Faker::Games::Dota.item,
    date_of_purchase: Faker::Date.between(from: 9.days.ago, to: Date.today),
    asset_code: Faker::Alphanumeric.alpha(number: 10),
    category: Faker::Games::Dota.team,
    description: Faker::Games::Dota.quote
  })
end

