class CreateAssetitems < ActiveRecord::Migration[5.2]
  def change
    create_table :assetitems do |t|
      t.string :item
      t.date :date_of_purchase
      t.string :asset_code
      t.string :category
      t.text :description

      t.timestamps
    end
  end
end
