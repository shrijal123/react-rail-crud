class Api::V1::AssetitemsController < ApplicationController
  
  def index
    assetitems = Assetitem.all
    render json: {status: 'SUCCESS', message:'Loaded Assets', data:assetitems}, status: :ok
  end

  def show
    article = Assetitem.find(params[:id])
    render json: {status: 'SUCCESS', message:'Loaded article', data:article},status: :ok
  end

  def create
    article = Assetitem.new(article_params)

    if article.save
      render json: {status: 'SUCCESS', message:'Saved article', data:article},status: :ok
    else
      render json: {status: 'ERROR', message:'Article not saved', data:article.errors},status: :unprocessable_entity
    end
  end

  def destroy
    article = Assetitem.find(params[:id])
    article.destroy
    render json: {status: 'SUCCESS', message:'Deleted article', data:article},status: :ok
  end

  def update
    article = Assetitem.find(params[:id])
    if article.update_attributes(article_params)
      render json: {status: 'SUCCESS', message:'Updated article', data:article},status: :ok
    else
      render json: {status: 'ERROR', message:'Article not updated', data:article.errors},status: :unprocessable_entity
    end
  end

  private

  def article_params
    params.permit(:item, :date_of_purchase, :asset_code, :category, :description)
  end

end