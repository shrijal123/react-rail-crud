class Assetitem < ApplicationRecord
  validates :item, presence: true
  validates :date_of_purchase, presence: true
  validates :asset_code, presence: true
end
